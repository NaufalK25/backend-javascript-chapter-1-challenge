const readline = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout,
});
const Aritmatika = require("./Aritmatika");
const { Persegi, Kubus, Tabung } = require("./Bentuk");

console.log('Kalkulator Sederhana!');
console.log('1. Tambah');
console.log('2. Kurang');
console.log('3. Kali');
console.log('4. Bagi');
console.log('5. Akar');
console.log('6. Kuadrat');
console.log('7. Luas Persegi');
console.log('8. Volume Kubus');
console.log('9. Volume Tabung');

readline.question("Input angka pertama: ", (angka1) => {
    readline.question("Input angka kedua: ", (angka2) => {
        readline.question("Input operator sesuai angka: ", (operator) => {
            const aritmatika = new Aritmatika(parseInt(angka1), parseInt(angka2));
            const persegi = new Persegi(parseInt(angka1));
            const kubus = new Kubus(parseInt(angka1));
            const tabung = new Tabung(parseInt(angka1), parseInt(angka2));

            const OPERASI = {
                '1': aritmatika.tambah(),
                '2': aritmatika.kurang(),
                '3': aritmatika.kali(),
                '4': aritmatika.bagi(),
                '5': aritmatika.akar(),
                '6': aritmatika.kuadrat(),
                '7': persegi.luas(),
                '8': kubus.volume(),
                '9': tabung.volume(),
            };

            console.log(`${OPERASI[operator] || 'Operator tidak ditemukan'}`);

            readline.close();
        });
    });
});
