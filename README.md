# Backend JavaScript Chapter 1 Challenge

Challenge for Chapter 1 from Binar Academy - Backend JavaScript class

### Prerequisites

1. [Git](https://git-scm.com/downloads)
    ```
    git --version
    ```
2. [Node.js](https://nodejs.org/en/)
    ```
    node -v
    ```

### Run In Terminal

1. Clone the repository

    ```
    git clone https://gitlab.com/NaufalK25/backend-javascript-chapter-1-challenge.git
    ```

2. Run using node command

    ```
    node index.js
    ```

### Question

1. [Simple calculator for arithmetic operations](Aritmatika.js)
    - Addition
    - Subtraction
    - Multiplication
    - Division
    - Root
    - Square
2. [Shape Calculator](Bentuk.js)
    - Rectangle Area
    - Cube Volume
    - Tube Volume
