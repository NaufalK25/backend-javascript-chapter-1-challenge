class Aritmatika {
    constructor(a, b) {
        this.a = a;
        this.b = b;
    }

    tambah() {
        return `${this.a} + ${this.b}: ${this.a + this.b}`;
    }

    kurang() {
        return `${this.a} - ${this.b}: ${this.a - this.b}`;
    }

    kali() {
        return `${this.a} * ${this.b}: ${this.a * this.b}`;
    }

    bagi() {
        return `${this.a} / ${this.b}: ${this.a / this.b}`;
    }

    akar() {
        return `V${this.a}: ${this.a ** 0.5}\nV${this.b}: ${this.b ** 0.5}`;
    }

    kuadrat() {
        return `${this.a}^2: ${this.a ** 2}\n${this.b}^2: ${this.b ** 2}`;
    }
}

module.exports = Aritmatika;
