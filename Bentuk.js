class Persegi {
    constructor(sisi) {
        this.sisi = sisi;
    }

    luas() {
        return `Luas Persegi: ${this.sisi ** 2}`;
    }
}


class Kubus {
    constructor(sisi) {
        this.sisi = sisi;
    }

    volume() {
        return `Volume Kubus: ${this.sisi ** 3}`;
    }
}

class Tabung {
    constructor(jariJari, tinggi) {
        this.jariJari = jariJari;
        this.tinggi = tinggi;
    }

    volume() {
        return `Volume Tabung: ${Math.PI * (this.jariJari ** 2) * this.tinggi}`;
    }
}

module.exports = {
    Persegi,
    Kubus,
    Tabung,
};
